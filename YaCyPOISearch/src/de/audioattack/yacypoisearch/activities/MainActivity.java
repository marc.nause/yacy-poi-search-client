package de.audioattack.yacypoisearch.activities;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.ResourceProxy;
import org.osmdroid.api.IGeoPoint;
import org.osmdroid.util.BoundingBoxE6;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.ItemizedOverlay;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.OverlayManager;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import de.audioattack.yacypoisearch.R;
import de.audioattack.yacypoisearch.data.ItemData;
import de.audioattack.yacypoisearch.views.DrawingOverlay;
import de.audioattack.yacypoisearch.views.DrawingOverlay.Circle;
import de.audioattack.yacypoisearch.views.MyMapView;
import de.audioattack.yacypoisearch.xml.GeoRssParser;

public class MainActivity extends Activity {

    private static final int INITIAL_ZOOM_LEVEL = 4;
    private static final int AUTO_ZOOM_LEVEL = 15;
    private static final int INITIAL_LAT = 50000000;
    private static final int INITIAL_LON = 9000000;

    private static final String KEY_LON_E6 = "lonE6";
    private static final String KEY_LAT_E6 = "latE6";
    private static final String KEY_ZOOM = "zoom";
    private static final String KEY_AUTOMATIC_LOCATION_UPDATE = "automaticLocationUpdate";
    private static final String KEY_ITEMS = "items";

    private MyMapView mapView;

    private ArrayList<ItemData> overlayItems = null;

    private MyLocationListener ll;
    private DrawingOverlay drawingOverlay;

    /** Used for logging. */
    private static final String TAG = MainActivity.class.getSimpleName();

    private static final double E6 = 1e6d;

    /** Request to back-end to retrieve POI information. */
    private static final String REQUEST =
            "http://poi1.yacy.net/yacysearch_location.rss?"
            + "dom=alltext&maximumTime=1000&maximumRecords=5000&"
            + "query=%s&lon=%s&lat=%s&r=%s";

    /** Use dot as decimal separator (and not for example comma
     * on German devices. */
    private static final DecimalFormatSymbols DFS =
            new DecimalFormatSymbols();
    static {
        DFS.setDecimalSeparator('.');
    }

    /** We don't need exaggerated precision. */
    private static final NumberFormat FORMATTER =
            new DecimalFormat("##0.000####", DFS);

    /** {@inheritDoc} */
    @Override
    public final void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        initMapView(savedInstanceState);
        initSearchBox();
        ll = new MyLocationListener();
    }

    private void initSearchBox() {
        final EditText editText = (EditText) this.findViewById(R.id.searchBox);
        editText.setOnKeyListener(new OnKeyListener() {

            @Override
            public boolean onKey(
                    final View v, final int keyCode, final KeyEvent event) {

                if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER
                        && event.getAction() == KeyEvent.ACTION_UP) {
                    closeKeyboard(v);
                    search();
                    return true;
                } else {
                    return false;
                }
            }

        });
    }

    /**
     * Initializes MapView from saved Bundle or with default values.
     * @param savedInstanceState saved bundle or null for default values
     */
    @SuppressWarnings("unchecked")
    private void initMapView(final Bundle savedInstanceState) {
        mapView = (MyMapView) this.findViewById(R.id.mapview);
        mapView.setMultiTouchControls(true);
        // Don't enable or onTouchListener will not work as expected. :-\
        mapView.setBuiltInZoomControls(false);

        final MapController ctrl = mapView.getController();

        final int zoom;
        final GeoPoint center;

        if (savedInstanceState != null) {
            zoom = savedInstanceState.getInt(KEY_ZOOM, INITIAL_ZOOM_LEVEL);
            center = new GeoPoint(
                    savedInstanceState.getInt(KEY_LAT_E6, INITIAL_LAT),
                    savedInstanceState.getInt(KEY_LON_E6, INITIAL_LON)
                    );
            if (savedInstanceState.containsKey(KEY_ITEMS)) {
                fillOverlay(
                        (ArrayList<ItemData>) (ArrayList<? extends Parcelable>)
                        savedInstanceState.getParcelableArrayList(KEY_ITEMS));
            }
            mapView.setAutomaticLocationUpdatesEnabled(
                    savedInstanceState.getBoolean(
                            KEY_AUTOMATIC_LOCATION_UPDATE, true));
        } else {
            zoom = INITIAL_ZOOM_LEVEL;
            center = new GeoPoint(INITIAL_LAT, INITIAL_LON);
        }

        ctrl.setZoom(zoom);
        ctrl.setCenter(center);

        drawingOverlay = new DrawingOverlay(this);
        mapView.getOverlayManager().add(drawingOverlay);

        mapView.setOnTouchListener(new OnTouchListener() {

            private boolean panning = false;
            private IGeoPoint oldMapCenter =
                    MainActivity.this.mapView.getMapCenter();

            @Override
            public boolean onTouch(final View view, final MotionEvent event) {

                final int action = event.getAction();
                if (action == MotionEvent.ACTION_UP && panning) {
                    panning  = false;
                    if (getDistance(oldMapCenter, mapView.getMapCenter())
                            > MainActivity.this.getRadius()) {
                        oldMapCenter = mapView.getMapCenter();

                        MainActivity.this.search();
                    }

                    return true;

                } else if (action == MotionEvent.ACTION_MOVE) {
                    panning = true;
                }

                return false;
            }

            private double getDistance(
                    final IGeoPoint point1, final IGeoPoint point2) {
                final double distLat =
                        point1.getLatitudeE6() - point2.getLatitudeE6();
                final double distLon =
                        point1.getLongitudeE6() - point2.getLongitudeE6();
                return
                        Math.sqrt(
                                (distLat * distLat) + (distLon * distLon)) / E6;
            }

        });

        // TODO: delete this, only for demo
        drawingOverlay.add(
                new Circle(
                        new GeoPoint(0,0),
                        100000,
                        null,
                        Color.argb(128, 255, 0, 0)));
    }

    @Override
    protected final void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_ZOOM, mapView.getZoomLevel());
        final IGeoPoint center = mapView.getMapCenter();
        outState.putInt(KEY_LON_E6, center.getLongitudeE6());
        outState.putInt(KEY_LAT_E6, center.getLatitudeE6());
        if (overlayItems != null) {
            outState.putParcelableArrayList(KEY_ITEMS, overlayItems);
        }
        outState.putBoolean(
                KEY_AUTOMATIC_LOCATION_UPDATE,
                mapView.isAutomaticLocationUpdatesEnabled());
    }

    @Override
    protected final void onPause() {
        // see: http://code.google.com/p/osmdroid/issues/detail?id=265
        mapView.getTileProvider().clearTileCache();

        ll.disable();

        super.onPause();
    }

    private void closeKeyboard(final View view) {
        InputMethodManager in =
                (InputMethodManager) getSystemService(
                        Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(view
                .getApplicationWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

    /** Start AsyncTask which sends request to back-end. */
    private void search() {
        final TextView textView = (TextView) this.findViewById(R.id.searchBox);
        final String searchText = textView.getText().toString();
        if (searchText != null && searchText.length() > 0) {
            new GeoSearch().execute(searchText);
        }
    }

    /**
     * Adds POI items to overlay. Overlay is cleared first.
     * @param items POIs to add.
     */
    private void fillOverlay(final List<ItemData> items) {

        overlayItems  = new ArrayList<ItemData>(items);

        final List<OverlayItem> list = new ArrayList<OverlayItem>(items.size());
        for (final ItemData itemData : items) {
            list.add(itemData.toOverlayItem());
        }

        final ResourceProxy mResourceProxy =
                new DefaultResourceProxyImpl(getApplicationContext());
        final OverlayManager om = mapView.getOverlayManager();
        om.clear();

        final ItemizedOverlay<OverlayItem> overlay =
                new ItemizedIconOverlay<OverlayItem>(
                        list,
                        new ItemizedIconOverlay.OnItemGestureListener<OverlayItem>() {
            @Override
            public boolean onItemSingleTapUp(final int index,
                    final OverlayItem item) {
                Toast.makeText(
                        MainActivity.this,
                        item.getTitle() + "\n" + item.getSnippet(),
                        Toast.LENGTH_SHORT).show();
                return true; // We 'handled' this event.
            }
            @Override
            public boolean onItemLongPress(final int index,
                    final OverlayItem item) {
//                Toast.makeText(
//                        DemoMap.this, 
//                        "Item '" + item.mTitle ,Toast.LENGTH_LONG).show();
                return false;
            }
        }, mResourceProxy);

        om.add(overlay);

        mapView.invalidate();
    }

    /**
     * Calculates radius for next request.
     * @return radius in degree
     */
    private double getRadius() {
        final BoundingBoxE6 bbox = mapView.getBoundingBox();

        final double lonSpan = bbox.getLongitudeSpanE6() / E6;
        final double latSpan = bbox.getLatitudeSpanE6() / E6;

        return Math.sqrt((lonSpan * lonSpan) + (latSpan * latSpan)) / 2d;
    }

    /**
     * Sends request to back-end, parses result and updates overlay.
     * @author low012
     */
    private class GeoSearch extends AsyncTask<String, Void, List<ItemData>> {

        private ProgressDialog progressDialog = null;

        @Override
        protected void onPreExecute() {

            if (!MainActivity.this.isFinishing()) {
                progressDialog =
                        ProgressDialog.show(
                                MainActivity.this,
                                null,
                                MainActivity.this.getString(
                                        R.string.loadMessage));
            }

            super.onPreExecute();
        }

        @Override
        protected List<ItemData> doInBackground(final String... args) {
            
            final List<ItemData> result = new ArrayList<ItemData>();

            final DefaultHttpClient client = new DefaultHttpClient();

            final IGeoPoint center = mapView.getMapCenter();

            HttpGet request;
            try {
                request =
                        new HttpGet(
                                String.format(
                                        REQUEST,
                                        URLEncoder.encode(args[0], "UTF-8"),
                                        FORMATTER.format(center.getLongitudeE6() / E6),
                                        FORMATTER.format(center.getLatitudeE6() / E6),
                                        FORMATTER.format(getRadius())));
            } catch (UnsupportedEncodingException e1) {
                Log.e(TAG, e1.getMessage());
                return new ArrayList<ItemData>();
            }
            request.setHeader("Accept-Encoding", "gzip");

            Log.d(TAG, request.getURI().toString());

            try {
                final HttpResponse response = client.execute(request);

                InputStream content = response.getEntity().getContent();
                final Header contentEncoding =
                        response.getFirstHeader("Content-Encoding");
                if (contentEncoding != null
                        && contentEncoding.getValue().equalsIgnoreCase("gzip")) {
                    Log.d(TAG, "Result of request has been gzipped!");
                    content = new GZIPInputStream(content);
                }

                final GeoRssParser parser = new GeoRssParser();
                parser.parse(content);
                result.addAll(parser.getResult());

            } catch (Exception e) {
                e.printStackTrace();
            }

            Log.d(TAG, "Returned items: " + result.size());

            return result;
        }

        @Override
        protected void onPostExecute(final List<ItemData> result) {

            if (progressDialog != null) {
                progressDialog.dismiss();
            }

            MainActivity.this.fillOverlay(result);

        }

    }

    /**
     * location listener to follow movements with new search results
     *
     */
    private final class MyLocationListener implements LocationListener {

        private LocationManager lm;

        public MyLocationListener() {
            lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            registerProviders();
        }

        @Override
        public void onLocationChanged(final Location loc) {
            if (!mapView.isAutomaticLocationUpdatesEnabled()) return;
            double lat, lon;
            if (loc != null && ((lat = loc.getLatitude()) != 0.0d) && ((lon = loc.getLongitude()) != 0.0d)) {
                final MapController ctrl = mapView.getController();
                GeoPoint p = new GeoPoint((int) (lat * E6), (int) (lon * E6));
                ctrl.setZoom(AUTO_ZOOM_LEVEL);
                ctrl.animateTo(p);
                Log.d(TAG, "map moved by location update to lat = " + p.getLatitudeE6() + ", lon = " + p.getLongitudeE6() + ", zoom = " + AUTO_ZOOM_LEVEL);
            }
        }

        @Override
        public void onProviderDisabled(String arg0) {
            
        }

        @Override
        public void onProviderEnabled(String arg0) {
            registerProviders();
        }

        @Override
        public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
            
        }

        private void registerProviders() {
            if (lm != null) {
                lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000 /*milliseconds*/, 10 /*meter*/, this);
                lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000 /*milliseconds*/, 10 /*meter*/, this);
            }
        }

        public void disable() {
            lm.removeUpdates(ll);
        }

        public void enable() {
            registerProviders();
        }

    }


}
