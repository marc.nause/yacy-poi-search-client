package de.audioattack.yacypoisearch.xml;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.osmdroid.util.GeoPoint;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import de.audioattack.yacypoisearch.data.ItemData;

public class GeoRssParser extends DefaultHandler {

    private static final String SUMMARY = "summary";
    private static final String TITLE = "title";
    private static final String ITEM = "item";
    private static final String LAT = "lat";
    private static final String LONG = "long";

    final SAXParserFactory factory;
    final SAXParser saxParser;
    private ArrayList<ItemData> result;

    private double lon;
    private double lat;
    private String title;
    private String summary;

    private boolean inLon = false;
    private boolean inLat = false;
    private boolean inTitle = false;
    private boolean inSummary = false;

    public GeoRssParser() throws ParserConfigurationException, SAXException {
        factory = SAXParserFactory.newInstance();
        saxParser = factory.newSAXParser();
    }

    public final void parse(final InputStream input)
            throws SAXException, IOException {
                result = new ArrayList<ItemData>();
                saxParser.parse(input, this);
            }

    @Override
    public final void startElement(
            final String uri, final String localName, final String qName,
            final Attributes attributes) throws SAXException {

        if (localName.equalsIgnoreCase(LONG)) {
            inLon = true;
        } else if (localName.equalsIgnoreCase(LAT)) {
            inLat = true;
        } else if (localName.equalsIgnoreCase(ITEM)) {
            lat = 0d;
            lon = 0d;
            title = "";
            summary = "";
        } else if (localName.equalsIgnoreCase(TITLE)) {
            inTitle = true;
        } else if (localName.equalsIgnoreCase(SUMMARY)) {
            inSummary = true;
        }

    }

    @Override
    public final void endElement(
            final String uri, final String localName, final String qName)
            throws SAXException {

        if (localName.equalsIgnoreCase(LONG)) {
            inLon = false;
        } else if (localName.equalsIgnoreCase(LAT)) {
            inLat = false;
        } else if (localName.equalsIgnoreCase(TITLE)) {
            inTitle = false;
        } else if (localName.equalsIgnoreCase(SUMMARY)) {
            inSummary = false;
        } else if (localName.equalsIgnoreCase(ITEM)) {
            result.add(new ItemData(
                    title,
                    summary,
                    new GeoPoint(lat, lon)
                    ));
        }
    }

    @Override
    public final void characters(
            final char[] ch, final int start, final int length)
            throws SAXException {

        if (inLon) {
            lon = Double.parseDouble(new String(ch, start, length));
        } else if (inLat) {
            lat = Double.parseDouble(new String(ch, start, length));
        } else if (inTitle) {
            title = new String(ch, start, length);
        } else if (inSummary) {
            summary = new String(ch, start, length);
        }
    }

    public final List<ItemData> getResult() {
        return result;
    }
}
